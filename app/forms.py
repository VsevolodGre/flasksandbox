from flask_wtf import FlaskForm
from wtforms import StringField, FloatField
from wtforms.validators import DataRequired


class AddRestaurantForm(FlaskForm):
    name = StringField('name', validators=[DataRequired()])
    country = StringField('country', validators=[DataRequired()])


class AddDishForm(FlaskForm):
    restaurant = StringField('restaurant', validators=[DataRequired()])
    name = StringField('name', validators=[DataRequired()])
    price = FloatField('price', validators=[DataRequired()])
