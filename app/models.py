from app import db


class RestaurantModel(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    country = db.Column(db.String(length=20), index=True, unique=False)
    name = db.Column(db.String(length=20), index=True, unique=True)
    dishes = db.relationship('DishModel', backref='restaurant', lazy='dynamic')

    def __repr__(self):
        return self.name + ' in ' + self.country

    def to_json(self):
        return {'name': self.name, 'country': self.country, 'price': str(self.get_average_price())}

    def get_average_price(self):
        if len(self.dishes.all()):
            average = 0
            for dish in self.dishes:
                average += dish.price
            return average / len(self.dishes.all())
        else:
            return 0


class DishModel(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(length=20), index=True, unique=False)
    price = db.Column(db.Float(precision=2), index=True, unique=False)
    restaurant_id = db.Column(db.Integer, db.ForeignKey('restaurant_model.id'))

    def __repr__(self):
        return self.name + ' for ' + self.price

    def to_json(self):
        return {'name': self.name, 'price': str(self.price)}



