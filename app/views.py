from app import app, db
from flask import render_template, redirect
import json
from .forms import AddRestaurantForm, AddDishForm
from .models import RestaurantModel, DishModel


@app.route('/')
@app.route('/index')
def index():
    return 'Hello World!'


@app.route('/restaurants', methods=['GET'])
def restaurants():
    all_restaurants = RestaurantModel.query.all()
    return json.dumps([restaurant.to_json() for restaurant in all_restaurants])


@app.route('/restaurants/<string:country_name>', methods=['GET'])
def restaurants_in_country(country_name):
    local_restaurants = RestaurantModel.query.filter_by(country=country_name)
    return json.dumps([restaurant.to_json() for restaurant in local_restaurants])


@app.route('/dishes', methods=['GET'])
def dishes():
    all_dishes = DishModel.query.all()
    return json.dumps([dish.to_json() for dish in all_dishes])


@app.route('/dishes/<string:restaurant_name>', methods=['GET'])
def dishes_in_restaurant(restaurant_name):
    restaurant = RestaurantModel.query.filter_by(name=restaurant_name).first()
    if restaurant:
        local_dishes = restaurant.dishes.all()
        return json.dumps([dish.to_json() for dish in local_dishes])
    else:
        return json.dumps('[]')


@app.route('/find/<string:country>', methods=['GET'])
def find_local(country):
    local_restaurants = RestaurantModel.query.filter_by(country=country).all()
    if local_restaurants:
        prices = []
        for r in local_restaurants:
            prices.append(r.get_average_price())
        return json.dumps(local_restaurants[prices.index(min(prices))].to_json())
    else:
        return json.dumps('[]')


@app.route('/add_restaurant', methods=['GET', 'POST'])
def add_restaurant():
    form = AddRestaurantForm()
    if form.validate_on_submit():
        restaurant = RestaurantModel(country=form.country.data, name=form.name.data)
        db.session.add(restaurant)
        db.session.commit()
        return redirect('/restaurants')
    else:
        return render_template('restaurant.html', form=form, title='Add Restaurant')


@app.route('/add_dish', methods=['GET', 'POST'])
def add_dish():
    form = AddDishForm()
    if form.validate_on_submit():
        restaurant = db.session.query(RestaurantModel).filter_by(name=form.restaurant.data).first()
        if restaurant is not None:
            dish = DishModel(name=form.name.data, price=form.price.data, restaurant_id=restaurant.id)
            db.session.add(dish)
            db.session.commit()
        return redirect('/dishes')
    else:
        return render_template('dish.html', form=form, title='Add Dish')


@app.route('/clear')
def clear():
    all_dishes = DishModel.query.all()
    all_restaurants = RestaurantModel.query.all()
    for dish in all_dishes:
        db.session.delete(dish)
    for restaurant in all_restaurants:
        db.session.delete(restaurant)
    db.session.commit()
    return 'db is clean'
