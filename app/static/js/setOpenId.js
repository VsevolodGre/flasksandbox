/**
 * Created by Vsevolod on 26.02.2017.
 */
function setOpenId(openId, provider)
{
    var u = openId.search('<username>');
    if (u != -1) {
        var user = prompt('Enter your username for ' + provider);
        openId = openId.substr(0, u) + user;
    }
    document.forms['login'].elements['openid'].value = openId;
}